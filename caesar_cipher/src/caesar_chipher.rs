pub struct CaesarCipher {
    shift: u8,
}

impl CaesarCipher {
    pub fn new(shift: u8) -> CaesarCipher {
        CaesarCipher { shift: shift % 26 }
    }

    pub fn encrypt(&self, input: &str) -> String {
        input
            .chars()
            .map(|c| self.shift_char(c, self.shift))
            .collect()
    }

    pub fn decrypt(&self, input: &str) -> String {
        input
            .chars()
            .map(|c| self.shift_char(c, 26 - self.shift))
            .collect()
    }

    fn shift_char(&self, c: char, shift: u8) -> char {
        if c.is_ascii_alphabetic() {
            let base = if c.is_ascii_lowercase() { b'a' } else { b'A' };
            let offset = (c as u8 - base + shift) % 26;
            (base + offset) as char
        } else {
            c
        }
    }
}
