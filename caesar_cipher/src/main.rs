mod caesar_chipher;
use caesar_chipher::CaesarCipher;

fn main() {
    let cipher = CaesarCipher::new(8);
    let text = "Hello, World!";

    let encrypted = cipher.encrypt(text);
    println!("Encrypted: {}", encrypted);

    let decrypted = cipher.decrypt(&encrypted);
    println!("Decrypted: {}", decrypted);
}
