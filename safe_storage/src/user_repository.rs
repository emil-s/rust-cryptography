use crate::password_hasher::PasswordHasher;
use sqlx::mysql::MySqlPool;

pub struct UserRepository {
    pool: MySqlPool,
}

impl UserRepository {
    pub async fn new(database_url: &str) -> Result<Self, sqlx::Error> {
        let pool = MySqlPool::connect(database_url).await?;
        Ok(Self { pool })
    }

    pub async fn create_table(&self) -> Result<(), sqlx::Error> {
        sqlx::query!(
            r#"
            CREATE TABLE IF NOT EXISTS users (
                username VARCHAR(50) PRIMARY KEY,
                hash VARCHAR(255) NOT NULL,
                salt VARCHAR(255) NOT NULL
            );
            "#,
        )
        .execute(&self.pool)
        .await?;

        Ok(())
    }

    pub async fn insert_user(&self, username: &str, password: &str) -> Result<(), sqlx::Error> {
        let salt = PasswordHasher::generate_salt();
        let hash = PasswordHasher::hash_password(password, &salt);

        sqlx::query!(
            r#"
            INSERT INTO users (username, hash, salt)
            VALUES (?, ?, ?);
            "#,
            username,
            &hash,
            &salt
        )
        .execute(&self.pool)
        .await?;
        Ok(())
    }

    pub async fn select_user(
        &self,
        username: &str,
        password: &str,
    ) -> Result<Option<User>, sqlx::Error> {
        let res = sqlx::query_as!(
            User,
            r#"
            SELECT username, hash, salt
            FROM users
            WHERE username = ?;
            "#,
            username
        )
        .fetch_optional(&self.pool)
        .await?;
        if let Some(ref user) = res {
            if !PasswordHasher::verify_password(password, &user.salt, &user.hash) {
                return Ok(None); // Password doesn't match
            }
        }

        Ok(res)
    }
}

#[derive(Debug)]
pub struct User {
    username: String,
    hash: String,
    salt: String,
}
