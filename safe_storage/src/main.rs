mod password_hasher;
mod user_repository;

use user_repository::UserRepository;

#[tokio::main]
async fn main() -> Result<(), sqlx::Error> {
    dotenv::dotenv().ok();
    let database_url = std::env::var("DATABASE_URL").expect("DATABASE_URL must be set");
    let repository = UserRepository::new(&database_url).await?;

    repository.create_table().await?;
    repository
        .insert_user("Emil", "My secret password.")
        .await?;
    let user = repository.select_user("Emil", "My secret password").await?;

    println!("User: {:?}", user);

    Ok(())
}
