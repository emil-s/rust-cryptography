use rand::rngs::OsRng;
use rand::Rng;

pub fn secure_random() -> Vec<u8> {
    let mut rng = OsRng;
    (0..1_000_000).map(|_| rng.gen()).collect()
}

pub fn pseudo_random() -> Vec<u8> {
    let mut rng = rand::thread_rng();
    (0..1_000_000).map(|_| rng.gen()).collect()
}
