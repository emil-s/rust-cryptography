use criterion::{criterion_group, criterion_main, Criterion};
use random_numbers::*;

fn benchmark_secure(c: &mut Criterion) {
    c.bench_function("Secure RNG", |b| b.iter(|| secure_random()));
}

fn benchmark_pseudo(c: &mut Criterion) {
    c.bench_function("Pseudo RNG", |b| b.iter(|| pseudo_random()));
}

criterion_group!(benches, benchmark_secure, benchmark_pseudo);
criterion_main!(benches);
