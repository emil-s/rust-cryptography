mod rsa_key;

use rsa_key::RsaKey;
use std::sync::Arc;
use tokio::io::AsyncReadExt;
use tokio::net::{TcpListener, TcpStream};

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    // Create or load the RSA key
    let key = Arc::new(RsaKey::new());

    // Display the public key
    key.display_public_key();

    // Start the TCP listener
    let listener = TcpListener::bind("127.0.0.1:8080").await?;
    println!("Listening on 127.0.0.1:8080...");

    loop {
        let (socket, _) = listener.accept().await?;
        let key = Arc::clone(&key); // Clone the Arc

        // Spawn a new task to handle the connection
        tokio::spawn(handle_connection(socket, key));
    }
}

async fn handle_connection(mut socket: TcpStream, key: Arc<RsaKey>) {
    let mut buf = vec![0u8; 1024];
    loop {
        match socket.read(&mut buf).await {
            Ok(n) => {
                if n == 0 {
                    return;
                }

                let encrypted_data = buf[..n].to_vec();
                let decrypted_data = key.decrypt(&encrypted_data);

                match decrypted_data {
                    Some(data) => println!("Decrypted data: {}", String::from_utf8_lossy(&data)),
                    None => println!("Failed to decrypt data"),
                }
            }
            Err(e) => {
                println!("Error reading from socket: {}", e);
                return;
            }
        }
    }
}
