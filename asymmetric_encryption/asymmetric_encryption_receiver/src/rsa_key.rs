use rand::rngs::OsRng;
use rsa::{traits::PublicKeyParts, Pkcs1v15Encrypt, RsaPrivateKey, RsaPublicKey};

pub struct RsaKey {
    private_key: RsaPrivateKey,
    public_key: RsaPublicKey,
}

impl RsaKey {
    pub fn new() -> Self {
        let bits = 2048;
        let private_key = RsaPrivateKey::new(&mut OsRng, bits).expect("Failed to generate a key");
        let public_key = RsaPublicKey::from(&private_key);

        RsaKey {
            private_key,
            public_key,
        }
    }

    pub fn display_public_key(&self) {
        // the {:x} part causes the keys to printed out in hexadecimal.
        println!("Public Exponent: {:x}", self.public_key.e());
        println!("Public Modulus: {:x}", self.public_key.n());
    }

    pub fn decrypt(&self, ciphertext: &[u8]) -> Option<Vec<u8>> {
        let padding = Pkcs1v15Encrypt::default();
        self.private_key.decrypt(padding, ciphertext).ok()
    }
}
