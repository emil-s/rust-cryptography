mod rsa_key;
use base64::{engine::general_purpose, Engine};
use rsa_key::RsaKey;
use std::io::{self, Write};
use tokio::io::AsyncWriteExt;
use tokio::net::TcpStream;

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    let (e, n) = get_key_parts()?;
    let key = RsaKey::from_parts(&e, &n)?;

    loop {
        let message = get_user_input("Enter message to encrypt: ")?;
        match key.encrypt(message.as_bytes()) {
            Some(data) => {
                println!(
                    "Encrypted data: {}",
                    general_purpose::STANDARD.encode(&data)
                );
                let mut socket = TcpStream::connect("127.0.0.1:8080").await?;
                socket.write_all(&data).await?;
            }
            None => println!("Failed to encrypt message"),
        }
    }
}

fn get_user_input(prompt: &str) -> Result<String, io::Error> {
    print!("{}", prompt);
    io::stdout().flush()?;
    let mut input = String::new();
    io::stdin().read_line(&mut input)?;
    Ok(input.trim().to_string())
}

fn get_key_parts() -> Result<(String, String), io::Error> {
    let e = get_user_input("Enter public Exponent (in hex): ")?;
    let n = get_user_input("Enter public Modulus (in hex): ")?;
    Ok((e, n))
}
