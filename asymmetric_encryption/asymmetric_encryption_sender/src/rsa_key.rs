use rsa::{BigUint, Pkcs1v15Encrypt, RsaPublicKey};

pub struct RsaKey {
    public_key: RsaPublicKey,
}

impl RsaKey {
    pub fn from_parts(e: &str, n: &str) -> Result<Self, Box<dyn std::error::Error>> {
        // Makes sure that the length of the hexadecimal input is an even number.
        let decode_hex = |s: &str| {
            hex::decode(match s.len() % 2 == 0 {
                true => s.into(),
                false => format!("0{}", s),
            })
        };

        let e_bytes = decode_hex(e)?;
        let n_bytes = decode_hex(n)?;
        let e = BigUint::from_bytes_be(&e_bytes);
        let n = BigUint::from_bytes_be(&n_bytes);

        // I construct my public key from the input shared by the receiver.
        let public_key = RsaPublicKey::new(n, e)?;

        Ok(RsaKey { public_key })
    }

    pub fn encrypt(&self, plaintext: &[u8]) -> Option<Vec<u8>> {
        self.public_key
            .encrypt(
                &mut rand::thread_rng(),
                Pkcs1v15Encrypt::default(),
                plaintext,
            )
            .ok()
    }
}
