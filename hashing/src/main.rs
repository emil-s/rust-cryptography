mod mac_handler;

use mac_handler::{Algorithm, MACHandler};

fn main() {
    // I append my str with a b, in order to turn it into a byte array.
    let handler = MACHandler::new(Algorithm::SHA512, b"my hashing key");

    let password = b"my secret password";
    let mac = handler.compute_mac(password);

    println!("The hash is: {:?}", mac);

    println!(
        "With the correct hash: {}",
        handler.check_authenticity(password, &mac[..])
    );
    println!(
        "With an incorrect hash: {}",
        handler.check_authenticity(password, b"this is 100% not the right hash.")
    );
}
