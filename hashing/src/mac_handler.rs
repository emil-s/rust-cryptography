use ring::hmac;

pub struct MACHandler {
    key: hmac::Key,
}

pub enum Algorithm {
    SHA1,
    SHA256,
    SHA384,
    SHA512,
}

impl MACHandler {
    pub fn new(algorithm: Algorithm, key: &[u8]) -> MACHandler {
        let alg = match algorithm {
            Algorithm::SHA1 => hmac::HMAC_SHA1_FOR_LEGACY_USE_ONLY,
            Algorithm::SHA256 => hmac::HMAC_SHA256,
            Algorithm::SHA384 => hmac::HMAC_SHA384,
            Algorithm::SHA512 => hmac::HMAC_SHA512,
        };
        MACHandler {
            key: hmac::Key::new(alg, key),
        }
    }

    pub fn compute_mac(&self, message: &[u8]) -> Vec<u8> {
        hmac::sign(&self.key, message).as_ref().to_vec()
    }

    pub fn check_authenticity(&self, message: &[u8], mac: &[u8]) -> bool {
        hmac::verify(&self.key, message, mac).is_ok()
    }
}
